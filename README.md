# nick-winter

My intention for this project is to be an online profile, displaying my interests hobbies skills and background, this will be an on-going project for the forseeable future. In order to run the project please complete the following steps:

## Clone the Project
```
git clone https://NickWT@bitbucket.org/NickWT/nick-winter.git
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```