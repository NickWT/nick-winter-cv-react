import React from 'react';
import '../styles/cvSkills.css';
import CvLoader from '../components/cvLoader.js';


class cvSkills extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
    this.loading = this.loading.bind(this);
  }

  loading() {
    setTimeout(() => {
      this.setState(() => ({
        loading: false,
      }));
    }, 1000);
  }

  componentDidMount() {
    this.loading();
  }

  render() {
    if (this.state.loading) {
      return(
        <div className="skills-main-content">
          <div className="row skills-title">
            <CvLoader type="xl-text"></CvLoader>
          </div>
          <div className="skills-content-container">
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
            <div className="skill-title">
              <CvLoader type="xl-text"></CvLoader>
            </div>
            <div className="skill-container">
              <CvLoader type="xxxl-text"></CvLoader>
            </div>
          </div>
        </div>
      );
    } else {
      return(
        <div className="skills-main-content">
          <div v-if="!is.Loading">
            <div className="row skills-title">
              <h1><strong>Skills</strong></h1>
            </div>
            <div className="skills-content-container">
              <div className="skill-title">
                <h5>HTML</h5>
              </div>
              <div className="skill-container">
                <div className="skills eighty-percent">80%</div>
              </div>
              <div className="skill-title">
                <h5>CSS</h5>
              </div>
              <div className="skill-container">
                <div className="skills eighty-percent">80%</div>
              </div>
              <div className="skill-title">
                <h5>Javascript</h5>
              </div>
              <div className="skill-container">
                <div className="skills seventy-percent">70%</div>
              </div>
              <div className="skill-title">
                <h5>PHP</h5>
              </div>
              <div className="skill-container">
                <div className="skills fifty-five-percent">55%</div>
              </div>
              <div className="skill-title">
                <h5>SQL</h5>
              </div>
              <div className="skill-container">
                <div className="skills fifty-percent">50%</div>
              </div>
              <div className="skill-title">
                <h5>YAML</h5>
              </div>
              <div className="skill-container">
                <div className="skills fifty-percent">50%</div>
              </div>
              <div className="skill-title">
                <h5>Java</h5>
              </div>
              <div className="skill-container">
                <div className="skills sixty-five-percent">65%</div>
              </div>
              <div className="skill-title">
                <h5>Vue JS</h5>
              </div>
              <div className="skill-container">
                <div className="skills seventy-five-percent">75%</div>
              </div>
              <div className="skill-title">
                <h5>Node JS</h5>
              </div>
              <div className="skill-container">
                <div className="skills seventy-percent">70%</div>
              </div>
              <div className="skill-title">
                <h5>Express JS</h5>
              </div>
              <div className="skill-container">
                <div className="skills seventy-percent">70%</div>
              </div>
              <div className="skill-title">
                <h5>Jest</h5>
              </div>
              <div className="skill-container">
                <div className="skills fifty-percent">50%</div>
              </div>
              <div className="skill-title">
                <h5>Git</h5>
              </div>
              <div className="skill-container">
                <div className="skills eighty-percent">80%</div>
              </div>
              <div className="skill-title">
                <h5>Bitbucket</h5>
              </div>
              <div className="skill-container">
                <div className="skills eighty-percent">80%</div>
              </div>
              <div className="skill-title">
                <h5>Github</h5>
              </div>
              <div className="skill-container">
                <div className="skills seventy-percent">70%</div>
              </div>
              <div className="skill-title">
                <h5>AWS</h5>
              </div>
              <div className="skill-container">
                <div className="skills thirty-percent">30%</div>
              </div>
              <div className="skill-title">
                <h5>Jira</h5>
              </div>
              <div className="skill-container">
                <div className="skills eighty-percent">80%</div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  componentWillUnmount() {
    return ('');
  }

}

export default cvSkills;