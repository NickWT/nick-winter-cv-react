import React from 'react';
import CvModal from '../components/cvModal.js';
import CvLoader from '../components/cvLoader.js';
import '../styles/cvExperiences.css';

var modalTitle = '';

class cvExperiences extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      showModal: false,
    };
    this.loading = this.loading.bind(this);
    this.displayModal = this.displayModal.bind(this);
    this.onModalHide = this.onModalHide.bind(this);
  }

  loading() {
    setTimeout(() => {
      this.setState(() => ({
        loading: false,
      }));
    }, 1000);
  }

  displayModal(event) {
    modalTitle = event;
    this.setState(() => ({
      showModal: true,
    }));
  }

  onModalHide() {
    modalTitle = '';
    this.setState(() => ({
      showModal: false,
    }));
  }

  componentDidMount() {
    this.loading();
  }

  render() {
    if (this.state.loading) {
      return(
        <div className="experience-main-content">
          <div className="row justify-content-md-center experience-title">
            <CvLoader type="lg-text"></CvLoader>
          </div>
          <div className="timeline">
            <div className="timeline-container left">
              <div className="content">
                <CvLoader type="lg-text"></CvLoader>
                <br/>
                <CvLoader type="lg-text"></CvLoader>
                <CvLoader type="lg-text"></CvLoader>
              </div>
            </div>
            <div className="timeline-container right">
              <div className="content">
                <CvLoader type="lg-text"></CvLoader>
                <br/>
                <CvLoader type="lg-text"></CvLoader>
                <CvLoader type="lg-text"></CvLoader>
                <hr/>
                <CvLoader type="lg-text"></CvLoader>
                <CvLoader type="lg-text"></CvLoader>
              </div>
            </div>
            <div className="timeline-container left">
              <div className="content">
                <CvLoader type="lg-text"></CvLoader>
                <br/>
                <CvLoader type="lg-text"></CvLoader>
                <CvLoader type="lg-text"></CvLoader>
              </div>
            </div>
            <div className="timeline-container right">
              <div className="content">
                <CvLoader type="lg-text"></CvLoader>
                <br/>
                <CvLoader type="lg-text"></CvLoader>
                <CvLoader type="lg-text"></CvLoader>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="experience-main-content">
          <div className="row experience-title">
            <h1><strong>Experience</strong></h1>
          </div>
          <div className="timeline">
            <div className="timeline-container left" onClick={() => this.displayModal('Software Developer')}>
              <div className="content">
                <h2>2020</h2>
                <h5>January 27th 2020 – present</h5>
                <h5>Software Developer, Smartpay</h5>
              </div>
            </div>
            <div className="timeline-container right">
              <div className="content">
                <span onClick={() => this.displayModal('Level 2 IT Support Technician')}>
                  <h2>2019</h2>
                  <h5>September 2nd 2019 – January 27th 2020</h5>
                  <h5>Level 2 IT Support Technician, Smartpay</h5>
                </span>
                <hr />
                <span onClick={() => this.displayModal('(AUT) Intern Front-End Software Developer')}>
                  <h5>May 13th 2019 - October 22nd 2019</h5>
                  <h5>Intern Front-End Software Developer, Smartpay</h5>
                </span>
              </div>
            </div>
            <div className="timeline-container left" onClick={() => this.displayModal('Level 1 IT Support Technician')}>
              <div className="content">
                <h2>2018</h2>
                <h5>June 19th 2018 – September 2nd 2019</h5>
                <h5>Level 1 IT Support Technician, Smartpay</h5>
              </div>
            </div>
            <div className="timeline-container right" onClick={() => this.displayModal('Sales Assistant')}>
              <div className="content">
                <h2>2013</h2>
                <h5>August 16th 2013 – June 15th 2018</h5>
                <h5>Sales assistant, Bunnings Warehouse Silverdale</h5>
              </div>
            </div>
          </div>
          <CvModal
            modalTitle={modalTitle}
            show={this.state.showModal}
            onHide={this.onModalHide}
          >
            <div className="row experience-content">
              {modalTitle === 'Software Developer' &&
                <ul>
                  <li>Second promotion to a role with greater levels of responsibility and complexity</li>
                  <li>Contribute to creating high quality, streamlined and efficient software using Scrum agile framework</li>
                  <li>Work on multiple development activities, including design, implementation, testing and continuous integration</li>
                  <li>Prepare and maintained detailed documentation for all software programs</li>
                  <li>Assist with developing and maintaining internal systems</li>
                  <li><strong>Technologies:</strong> AWS – NodeJS – SQL – VueJS – HTML5 – JS – CSS – GoogleAPIv4 – Git – GitFlow – SourceTree – Bitbucket – Node JS</li>
                </ul>
              }
              {modalTitle === 'Level 2 IT Support Technician' &&
                <ul>
                  <li>Promoted to this role and in conjunction with Level 1 support activities, managed technical escalations</li>
                  <li>Played a role in driving process improvements, identifying any areas of inefficiency and making recommendations for improvements</li>
                  <li>Carried out advanced level troubleshooting and successfully resolved a number challenging technical problems</li>
                  <li>Proactively created a centralised knowledge base in Atlassian’s Confluence as a training resource for internal staff</li>
                  <li>Developed standard operating procedures for business processes</li>
                </ul>
              }
              {modalTitle === '(AUT) Intern Front-End Software Developer' &&
                <ul>
                  <li>Secured an Internship through Auckland University of Technology</li>
                  <li>Worked with technical experts and gained knowledge and skills in front-end design, implementation and testing on an internal technical support system – worked in a Scrum agile framework</li>
                  <li><strong>Technologies:</strong> Vue JS – HTML5 – JS – CSS – Google API v4 – Git – Git Flow – Source Tree – Bitbucket – Node JS</li>
                </ul>
              }
              {modalTitle === 'Level 1 IT Support Technician' &&
                <ul>
                  <li>Performed a range of technical support activities, including troubleshooting, installation and swap of equipment, and recording data using Filemaker</li>
                  <li>Ensured all customers received an exceptional service experience</li>
                </ul>
              }
              {modalTitle === 'Sales Assistant' &&
                <ul>
                  <li>Assisted customers with finding products and provided advice on product availability and use</li>
                  <li>Processed cash and card payments – worked on the checkout and cashed up the till</li>
                  <li>Responded to all customer enquiries and complaints quickly and effectively</li>
                </ul>
              }
            </div>
          </CvModal>
        </div>
      );
    }
  }

  componentWillUnmount() {
    return('');
  }
}

export default cvExperiences;