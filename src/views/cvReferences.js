import React from 'react';
import '../styles/cvReferences.css';
import CvModal from '../components/cvModal.js';
import CvLoader from '../components/cvLoader.js';
import gustavo from '../assets/Gustavo.png';
import anslem from '../assets/Anslem.png';
import carlos from '../assets/Carlos.png';
import kirk from '../assets/Kirk.png';
import sean from '../assets/sean.png';
import jobIcon from '../assets/icons8-role-male-48.png';
import emailIcon from '../assets/email.svg';
import phoneIcon from '../assets/phone.svg';

var modalTitle = '';
var job = '';
var email = '';
var mobile = '';

class cvReferences extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      showModal: false,
    };
    this.displayModal = this.displayModal.bind(this);
    this.onModalHide = this.onModalHide.bind(this);
  }

  displayModal(event) {
    modalTitle = event.target.name;
    job = event.target.attributes.job.value;
    email = event.target.attributes.email.value;
    mobile = event.target.attributes.mobile.value;
    this.setState(() => ({
      showModal: true,
    }));
  }

  onModalHide() {
    modalTitle = '';
    job = '';
    email = '';
    mobile = '';
    this.setState(() => ({
      showModal: false,
    }));
  }

  loading() {
    setTimeout(() => {
      this.setState(() => ({
        loading: false,
      }));
    }, 1000);
  }

  componentDidMount() {
    this.loading();
  }

  render() {
    if (this.state.loading) {
      return(
        <div className="references-main-content">
          <div className="row references-title">
            <CvLoader type="xl-text"></CvLoader>
          </div>
          <div className="row">
            <div className="col-1">
            </div>
            <div className="col-10 reference-content">
              <div className="row">
                <div className="col-2">
                </div>
                <div className="col-4 reference">
                  <CvLoader type="reference-icon"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                </div>
                <div className="col-4 reference">
                  <CvLoader type="reference-icon"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                </div>
                <div className="col-2">
                </div>
              </div>
              <div className="row">
                <div className="col-4 reference">
                  <CvLoader type="reference-icon"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                </div>
                <div className="col-4 reference">
                  <CvLoader type="reference-icon"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                </div>
                <div className="col-4 reference">
                  <CvLoader type="reference-icon"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                  <CvLoader type="text"></CvLoader>
                </div>
              </div>
            </div>
            <div className="col-1">
            </div>
          </div>
        </div>
      );
    } else {
      return(
        <div className="references-main-content">
          <div className="row references-title">
            <h1><strong>References</strong></h1>
          </div>
          <div className="row">
            <div className="col-1">
            </div>
            <div className="col-10 reference-content">
              <div className="row">
                <div className="col-2">
                </div>
                <div className="col-4 reference">
                  <img src={gustavo} className="referee-icons" onClick={this.displayModal} name="Gustavo" job="Head of Engineering" email="gustavo.herrera@smartpay.co.nz" mobile="0221364940" alt="Gustavo" />
                  <p><strong>Gustavo Herrera,</strong></p>
                  <p><strong>Smartpay Limited</strong></p>
                </div>
                <div className="col-4 reference">
                  <img src={anslem} className="referee-icons" onClick={this.displayModal} name="Anslem" job="Team Lead/SCRUM Master" email="anslem.cadelis@smartpay.co.nz" mobile="021393994" alt="Anslem" />
                  <p><strong>Anslem Cadelis,</strong></p>
                  <p><strong>Smartpay Limited</strong></p>
                </div>
                <div className="col-2">
                </div>
              </div>
              <div className="row">
                <div className="col-4 reference">
                  <img src={carlos} className="referee-icons" onClick={this.displayModal} name="Carlos" job="Senior Front-End Developer" email="carlos.rosa@smartpay.co.nz" mobile="" alt="Carlos" />
                  <p><strong>Carlos Rosa,</strong></p>
                  <p><strong>Smartpay Limited</strong></p>
                </div>
                <div className="col-4 reference">
                  <img src={kirk} className="referee-icons" onClick={this.displayModal} name="Kirk" job="Technical Help Desk Manager" email="kirk.delamore@smartpay.co.nz" mobile="0210442533" alt="Kirk" />
                  <p><strong>Kirk Delamore,</strong></p>
                  <p><strong>Smartpay Limited</strong></p>
                </div>
                <div className="col-4 reference">
                  <img src={sean} className="referee-icons" onClick={this.displayModal} name="Sean" job="Manager/Coordinator" email="" mobile="02102272498" alt="Sean" />
                  <p><strong>Sean Trent,</strong></p>
                  <p><strong>Bunnings Silverdale</strong></p>
                </div>
              </div>
            </div>
            <div className="col-1">
            </div>
          </div>
          <CvModal
            modalTitle={modalTitle}
            show={this.state.showModal}
            onHide={this.onModalHide}
          >
            <div className="container">
              <div className="row reference-overview">
                <ul>
                  <li><img src={jobIcon} className="icons" alt="job-icon" /><strong>{job}</strong></li>
                  <br />
                  {email !== '' &&
                    <li><a href={'mailto:' + email + '?Subject=Nick%20Winter%20Reference%20Check'} target="_top"><img src={emailIcon} className="icons" alt="email-icon" /></a><strong>{email}</strong></li>
                  }
                  <br />
                  {mobile !== '' &&
                    <li><a href={'tel:' + mobile}><img src={phoneIcon} className="icons" alt="phone-icon" /></a><strong>{mobile}</strong></li>
                  }
                </ul>
              </div>
            </div>
          </CvModal>
        </div>
      );
    }
  }

  componentWillUnmount() {
    return ('');
  }

}

export default cvReferences;