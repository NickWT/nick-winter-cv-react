import React from 'react';
import '../styles/cvAbout.css';
import CvLoader from '../components/cvLoader.js';
import profilePicture from '../assets/nickw.png';
import email from '../assets/email.svg';
import phone from '../assets/phone.svg';
import location from '../assets/location.svg';
import education from '../assets/icons8-student-male-48.png';
import bitbucket from '../assets/icons8-bitbucket-48.png';
import facebook from '../assets/icons8-facebook-f-48.png';
import linkedin from '../assets/icons8-linkedin-48.png';

class cvAbout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
    this.loading = this.loading.bind(this);
  }

  loading() {
    setTimeout(() => {
      this.setState(() => ({
        loading: false,
      }));
    }, 1000);
  }

  componentDidMount() {
    this.loading();
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="about-main-content">
          <div className="row">
            <div className="img-fluid about-profile_pic">
              <CvLoader type="profile-picture"/>
            </div>
          </div>
          <div className="row about-title">
            <div className="col-12 text-center">
              <CvLoader type="xl-text"/>
            </div>
          </div>
          <div className="row">
            <div className="col-2"></div>
            <div className="col-8 overview">
              <ul>
                <li><CvLoader type="xl-text"></CvLoader></li>
                <br/>
                <li><CvLoader type="xl-text"></CvLoader></li>
                <br/>
                <li><CvLoader type="xl-text"></CvLoader></li>
                <br/>
                <li><CvLoader type="xl-text"></CvLoader></li>
              </ul>
            </div>
            <div className="col-2"></div>
          </div>
          <div className="row social-icons">
            <div className="col-3"></div>
              <div className="col-6">
                <CvLoader className='social-loader-icons' type="image"></CvLoader>
                <CvLoader className='social-loader-icons' type="image"></CvLoader>
                <CvLoader className='social-loader-icons' type="image"></CvLoader>
              </div>
            <div className="col-3"></div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="about-main-content">
          <div className="row">
            <img src={profilePicture} className="img-fluid about-profile_pic" alt="profile"/>
          </div>
          <div className="row about-title">
            <div className="col-12 text-center">
              <h1><strong>Nicholas Winter</strong></h1>
            </div>
          </div>
          <div className="row">
            <div className="col-2">
            </div>
            <div className="col-8 overview">
              <ul>
                <li><a href="mailto:nickwintertaia@gmail.com?Subject=CV%20Contact%20Alert" target="_top"><img src={email} className="icons img-fluid" alt="email" /></a><strong>nickwintertaia@gmail.com</strong></li>
                <br />
                <li><a href="tel:0274929272"><img src={phone} className="icons img-fluid" alt="phone number" /></a><strong> 0274929272</strong></li>
                <br />
                <li><img src={location} className="icons img-fluid" alt="location" /><strong>Auckland, New Zealand</strong></li>
                <br />
                <li><a href="https://www.aut.ac.nz/study/study-options/engineering-computer-and-mathematical-sciences/courses/bachelor-of-computer-and-information-sciences/software-development-major"><img src={education} className="icons img-fluid" alt="Education" /></a><strong>Bachelor's Degree in Computer and Information Sciences </strong></li>
              </ul>
            </div>
            <div className="col-2">
            </div>
          </div>
          <div className="row social-icons">
            <div className="col-3"></div>
            <div className="col-6">
              <a href="https://bitbucket.org/dashboard/overview">
                <img src={bitbucket} title="Bitbucket" alt="Bitbucket" />
              </a>
              <a href="https://www.facebook.com/nick.winter.12">
                <img src={facebook} title="Facebook" alt="Facebook" />
              </a>
              <a href="https://www.linkedin.com/in/nicholas-winter-158a2616b/">
                <img src={linkedin} title="Linkedin" alt="Linkedin" />
              </a>
            </div>
            <div className="col-3"></div>
          </div>
        </div>
      );
    }
  };

  componentWillUnmount() {
    return('');
  }
}

export default cvAbout;