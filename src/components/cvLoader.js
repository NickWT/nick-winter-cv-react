import React from 'react';
import '../styles/cvLoader.css';

class cvLoader extends React.Component {

  render() {
    return (
      <div className={'cv-loader-wrapper ' + this.props.className}>
        <div className={
          this.props.type === 'text-sm' ? ' text-sm-loading':
          this.props.type === 'text' ? ' text-loading':
          this.props.type === 'lg-text' ? ' text-loading-large':
          this.props.type === 'xl-text' ? ' text-loading-extra-large':
          this.props.type === 'xxxl-text' ? ' text-loading-xxxl':
          this.props.type === 'image' ? ' image-loading':
          this.props.type === 'reference-icon' ? ' reference-icon-loading':
          this.props.type === 'profile-picture' ? ' profile-picture' : ''}>
        </div>
      </div>
    );
  }
}

export default cvLoader;