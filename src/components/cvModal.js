import React from 'react';
import '../styles/cvModal.css';

class cvModal extends React.Component {

  render() {
      return (
        <div>
          {this.props.show && 
          <div className="modal show">
            <div className="modal-blackscreen" onClick={this.props.onHide}>
            </div>
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5><strong>{this.props.modalTitle}</strong></h5>
                  <button type="button" className="close" onClick={this.props.onHide}>
                      <i className="fas fa-times"></i>
                    </button>
                  </div>
                <div className="modal-body">
                  {this.props.children}
                </div>
              </div>
            </div>
          </div>
          }
        </div>
      );
  }
}

export default cvModal;