import React from 'react';
import '../styles/cvFooter.css';

const CvFooter = () => {
  return (
    <div className="footer-wrapper">
      <div className="footer-content">
        <div className="row float-right">
          <p>Version 1.0</p>
        </div>
      </div>
    </div>
  );
}

export default CvFooter;