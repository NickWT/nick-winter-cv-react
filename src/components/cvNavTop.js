import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/cvNavTop.css';

const CvNavTop = () => {
  return (
    <div className="nav-top">
      <nav>
        <p className="cv">CV</p>
        <Link className="nav-item" to="/references">References</Link>
        <Link className="nav-item" to="/skills">Skills</Link>
        <Link className="nav-item" to="/experience">Experience</Link>
        <Link className="nav-item" to="/">About</Link>
      </nav>
    </div>
  );
}

export default CvNavTop;