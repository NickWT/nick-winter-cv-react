import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import About from './views/cvAbout.js';
import cvExperience from './views/cvExperiences.js';
import CvSkills from './views/cvSkills.js';
import CvReferences from './views/cvReferences.js';
import CvNavTop from './components/cvNavTop.js'
import CvFooter from './components/cvFooter.js'

function App() {
  return (
    <div className="App">
        <Router>
          <CvNavTop/>
            <div className="container">
              <Switch>
                <Route exact path="/" component={About}/>
                <Route exact path="/experience" component={cvExperience} />
                <Route exact path="/skills" component={CvSkills}/>
                <Route exact path="/references" component={CvReferences} />
              </Switch>
            </div>
          <CvFooter/>,
        </Router>
    </div>
  );
}

export default App;
